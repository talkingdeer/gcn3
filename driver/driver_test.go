package driver

import (
	"log"
	"testing"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

func TestDriver(t *testing.T) {
	log.SetOutput(GinkgoWriter)
	RegisterFailHandler(Fail)
	RunSpecs(t, "GCN3 GPU Driver")
}

var _ = Describe("Driver", func() {
	//var (
	//	storage *mem.Storage
	//	driver  *Driver
	//)
	//
	//BeforeEach(func() {
	//	storage = mem.NewStorage(4 * mem.GB)
	//	driver = NewDriver(nil)
	//})

})
